def simple_function():
    return "You have called simple_function"

def side_effect_function():
    # return "SKABLAM"
    raise FloatingPointError("A disastrous floating point error has occurred")


class SimpleClass(object):
    def explode(self):
        return "KABOOM!"


