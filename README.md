## Getting Started - mock testes

Seguindo o tutorial de implementação de testes com mock.Disponível em: https://medium.com/python-pandemonium/python-mocking-you-are-a-tricksy-beast-6c4a1f8d19b2



## Instalando dependências
- Crie uma virtualenv com python3 utilizando o seguinte comando: `python3 -m venv nome_venv`
- Ative a virtualenv `mock`: `. nome_venv/bin/activate`

- Instale as dependências contidas no arquivo requirements.txt: `pip install -r requirements.txt`

## Executando os testes:
Abaixo estão listadas duas formas de executar os testes.
- pytest: `pytest nome_arquivo.py`
- pytest-watch: `pytest-watch nome_arquivo.py`

## Leia mais sobre mock
- https://docs.python.org/3/library/unittest.mock.html
- https://docs.python.org/3/library/unittest.mock-examples.html
- https://blog.fugue.co/2016-02-11-python-mocking-101.html

## Leia mais sobre unittest
- https://docs.python.org/3/library/unittest.html

# Leia mais sobre pytest
- https://docs.pytest.org/en/latest/
