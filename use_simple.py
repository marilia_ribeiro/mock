from unittest import mock
import simple

# --- Funciton --- #
def use_simple_function():
    result = simple.simple_function()
    print(result)
# use_simple_function()

#quando tem um decorator mock.patch tem que passar um parâmetro para a função
@mock.patch('simple.simple_function')
def mock_simple_function(mock_simple_func):
    mock_simple_func.return_value = "You have mocked simple function"
    result = simple.simple_function()
    print(result)
# mock_simple_function()

@mock.patch('simple.simple_function')
def mock_simple_function_with_side_effect(mock_simple_func):
    mock_simple_func.side_effect = simple.side_effect_function
    result = simple.simple_function()
    print(result)
# mock_simple_function_with_side_effect()


# --- Class --- #
def use_simple_class():
    inst = simple.SimpleClass()
    print(inst.explode())
# use_simple_class()

@mock.patch('simple.SimpleClass')
def mock_simple_class(mock_class):
    # print(mock_class)
    # print(simple.SimpleClass)
    # inst = simple.SimpleClass()
    # print(inst)
    # print(mock_class.return_value)
    mock_class.return_value.explode.return_value = "BOO!"
    inst = simple.SimpleClass()
    result = inst.explode()
    print(result)
mock_simple_class()




